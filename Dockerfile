FROM maven:3.5-jdk-8-alpine as build
COPY . /app
WORKDIR /app
RUN mvn install

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/target/authors-0.0.1-SNAPSHOT.jar /app
CMD ["java -jar authors-0.0.1-SNAPSHOT.jar"]
