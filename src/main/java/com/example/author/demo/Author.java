package com.example.author.demo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@NoArgsConstructor(force=true)
@AllArgsConstructor
public class Author {

    private String id;
    private String name;
    private @JsonInclude(NON_NULL) String twitter;
    private @JsonInclude(NON_NULL) String blog;

}
