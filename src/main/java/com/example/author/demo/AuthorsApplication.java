package com.example.author.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class AuthorsApplication {

    public static void main(String[] args) { SpringApplication.run(AuthorsApplication.class, args); }
}