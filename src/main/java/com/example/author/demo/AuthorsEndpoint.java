package com.example.author.demo;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AuthorsEndpoint {

    /**
     * get all authors
     */
    @GetMapping(path = "/authors/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<Author> getAuthors() {
        List<Author> authors = new ArrayList<Author>();
        authors.add(new Author("1", "vijay", "https://twitter.com/vijaykintali", "No Blogs..."));
        authors.add(new Author("2", "vijay", "https://twitter.com/vijaykintali", "No Blogs..."));
        return authors;
    }

}
